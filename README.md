
# Tropical cyclone flood maps from historical tracks, using GeoClaw

Compute tropical cyclone-induced storm surge and derive flood maps and tide gauge time series.

The `tc_inund` package is built around the CLIMADA subpackage `climada_petals.hazard.tc_surge_geoclaw`.
As of February 2024, the subpackage is still under [review](https://github.com/CLIMADA-project/climada_petals/pull/109),
but the documentation and tutorial are already accessible in a preliminary version of
the [CLIMADA docs](https://climada-petals--109.org.readthedocs.build/en/109/tutorial/climada_hazard_TCSurgeGeoClaw.html).

The `tc_inund` package implements an easy API to run the GeoClaw surge model for **historical** TC track data from IBTrACS
for selected regions of interest, with selected input data sets for topography, and base sea level.

## Installation

This software is tested on the PIK cluster (with SLURM). While the software dependencies can be set up on any
platform (e.g, Ubuntu, Windows, macOS) that is supported by the CLIMADA Python package, the computational requirements
are too high for most notebooks and personal workstation machines. It is recommended to launch a separate job for each
individual tropical cyclone, with 16 cores and 64 GB of memory.

To set up an environment for the scripts in this repository, first install the Python project CLIMADA in a conda environment following
the [advanced installation instructions](https://climada-python.readthedocs.io/en/stable/guide/install.html#advanced-instructions)
as specified in the CLIMADA docs. Make sure to enforce Python version 3.9 to be
used as this is the only version supported by `pyfes` (see below).

Then install the `feature/tc_surge_geoclaw` branch from the `climada_petals` repository.

```shell
git clone https://github.com/CLIMADA-project/climada_petals.git
cd climada_petals
git checkout feature/tc_surge_geoclaw
```

Update the Conda environment with the specifications from Petals and activate it:

```shell
mamba env update -n climada_env -f requirements/env_climada.yml
mamba activate climada_env
mamba install -e ./
```

The GNU "make" utility and a Fortran compiler are needed to run this software. It has been tested with `gfortran` from
the GNU compiler collection version 7.3.0 and 13.2.0, but it should be possible to run it with different compilers,
e.g. `ifort` from Intel, or with different versions of the GNU compiler. Make sure, a Fortran compiler is available in
your setup, and set the environment variable `FC` to point to that compiler (cf. `slurm/tc_run_geoclaw.sh.template`).
You might want to install `gfortran` and `make` as a conda package like this:

```shell
(climada_env) $ conda install gfortran make
```

In addition to the basic functionality of `climada_petals.hazard.tc_surge_geoclaw`, this package makes use of [FES2014](https://github.com/CNES/aviso-fes) model outputs to derive event-specific base sea levels based
on astronomical tides, which depends on the `pyfes` conda package:

```shell
(climada_env) $ conda install pyfes -c fbriol
```

For some of the preprocessing steps for input data described below, you will need the command line tools `wget`, `curl`,
and `cdo`. Make sure that they are available in your environment if you do not already have all the preprocessed input
data available from a different source.

Finally, as this repository is structured as a Python package you can install it from the command line in the root directory of this repository:

```shell
(climada_env) $ pip install -e ./
```

## Input data

Before running any of the scripts, you need to obtain a number of input files.

### FES2014 model outputs

For the FES2014 model outputs, download the ini-files from
the [FES2014 git repository](https://github.com/CNES/aviso-fes/), and extract the corresponding FES2014 model data as
available from the AVISO FTP servers, using the script `python -m tc_inund.fes_download`.

### Topography and bathymetry

If you want to use the SRTM15+ combined topo-bathymetry data, download the NetCDF-file (6.2 GB large!) from
the [official web page](https://topex.ucsd.edu/pub/srtm15_plus/) and place it in the `input` directory as follows:

```shell
(climada_env) $ mkdir -p ./input/dem/srtm15plus/tiles/
(climada_env) $ pushd ./input/dem/srtm15plus/
(climada_env) $ curl https://topex.ucsd.edu/pub/srtm15_plus/SRTM15_V2.5.5.nc -o SRTM15_V2.5.5.nc
(climada_env) $ gdal_retile.py -targetDir tiles -ps 10000 2000 -co COMPRESS=DEFLATE -co TILED=YES -s_srs EPSG:4326 SRTM15_V2.5.5.nc
(climada_env) $ gdalbuildvrt index.vrt tiles/*.tif
(climada_env) $ popd
```

For an example of a more advanced combination of topography and bathymetry data sets, there are scripts to overlay
CoastalDEM and MERIT-DEM on top of SRTM15+ provided here: https://gitlab.pik-potsdam.de/tovogt/combined-coastal-dem
If you would like to use the combined DEM product with this project, the output files needs to be placed
in `input/dem/combined/combine_v2.1.vrt`.

### Satellite altimetry

You can download [gridded daily sea level from satellite observations](https://cds.climate.copernicus.eu/cdsapp#!/dataset/satellite-sea-level-global)
from the Copernicus climate data store (CDS) at 0.25 degree resolution. The recommended way to do download the data as one NetCDF file per month in `input/daily_zos/` is using the script `python -m tc_inund.zos_download`. The script needs
the climate data store API installed. Instructions are [here](https://cds.climate.copernicus.eu/api-how-to). You need to
freely register to CDS to download the data and accept
[here](https://cds.climate.copernicus.eu/cdsapp/#!/terms/licence-to-use-copernicus-products) the CDS license.
For time-merging the files, the script uses CDO. Ensure a thread-safe version of CDO, e.g. 
```
module load cdo/1.9.6/gnu-threadsafe
```
on the PIK Potsdam HPC.

For our purposes, the daily data needs to be aggregated to monthly averages, and reprojected to be relative to the EGM96
geoid (consistently with the DEM datasets mentioned above). The original satellite altimetry data is relative to the
GOCO05s geoid. To do the conversion from GOCO05s to EGM96, install the GOCE User Toolbox (GUT) following the
instructions on the [GUT homepage at ESA](https://earth.esa.int/eogateway/tools-download/gut-download). Then, run the
following commands to download the geoid definitions and derive a conversion file in `input/GOCO05s_sub_EGM96_900as.nc`:

```shell
(climada_env) $ pushd ./input/
(climada_env) $ wget http://icgem.gfz-potsdam.de/getmodel/gfc/f77139b673203554b2445a94818a4d35ca6e2b5fca1042177434b7e0b72cf4d1/GOCO05s.gfc
(climada_env) $ wget http://icgem.gfz-potsdam.de/getmodel/gfc/971b0a3b49a497910aad23cd85e066d4cd9af0aeafe7ce6301a696bed8570be3/EGM96.gfc
(climada_env) $ gut geoidheight_gf \
                    -InFile EGM96.gfc \
                    -Ellipse WGS84 \
                    -R -179.875:179.875,-89.875:89.875 \
                    -I 0.25,0.25 \
                    -OutFile EGM96_900as.nc
(climada_env) $ gut geoidheight_gf \
                    -InFile GOCO05s.gfc \
                    -Ellipse WGS84 \
                    -R -179.875:179.875,-89.875:89.875 \
                    -I 0.25,0.25 \
                    -OutFile GOCO05s_900as.nc
(climada_env) $ cdo sub GOCO05s_900as.nc EGM96_900as.nc GOCO05s_sub_EGM96_900as.nc
(climada_env) $ popd
```

Finally, run the script `python -m tc_inund.zos_prepare` to reproject and aggregate the data. The result is written
to `input/monthly_zos_aviso.nc`. This step can take several hours.

## Basic execution

The heart of this project is the script `python -m tc_inund.tc_run_geoclaw` which launches the GeoClaw solver for a
single tropical cyclone track in the IBTrACS database of historical storms. The script expects only a single input
parameter on the command line: the IBTrACS storm ID. However, in many applications, you will at least specify a region
of interest to reduce the computational requirements (using the `--bounds` parameter):

```shell
(climada_env) $ python -m tc_inund.tc_run_geoclaw 2003262N17254 --bounds B-122.07999,17.24871,-99.20202,36.85008
```

You can specify the location of virtual tide gauges to record water level time series during the simulation, or
change how base sea level is derived from satellite altimetry and astronomical tides. Use the `--help` parameter to
obtain a list of all supported parameters.

The main model outputs are GeoTIFF (`.tif`) files in subdirectories of the `output/` directory with the maximum surge
height over the storm life time for each grid cell in the region of interest.

When used on the PIK HPC, you should run the script at least one time on the PIK login nodes so that missing packages and data can be downloaded from the internet. Compute nodes are not connected to the internet. You can do a quick run with low resolution on the login node with one OpenMP thread.

 ```shell
(climada_env) $ export OMP_NUM_THREADS=1
(climada_env) $ python -m tc_inund.tc_run_geoclaw 2003262N17254 --dem srtm --resolution 300 --bounds B-122.07999,17.24871,-99.20202,36.85008
```

## Execution on a SLURM cluster

As Geoclaw is resource-hungry, you would for most cases write the command line arguments into a file and run a SLURM batch script.

An example SLURM sbatch script that runs
`tc_run_geoclaw.py` (see previous section) on SLURM is included in `slurm/tc_run_geoclaw.sh.template`. Copy the template
to `slurm/tc_run_geoclaw.sh`, open the file in a text editor, and follow the instruction in the file to replace all of
the placeholders according to your environment.

The batch script is designed to run a whole job array where each job in the array executes `tc_inund.tc_run_geoclaw`
with command line parameters taken from a single line in a predefined job file. Hence, to run the script, you generate
a file `jobs/my_args.txt` with each line containing one set of arguments for which you would like to run the Python
script `tc_inund.tc_run_geoclaw`. Assuming that your job file has 10 lines, you submit the script to SLURM as follows:

```bash
(climada_env) $ sbatch --qos standby --array 1-10 slurm/tc_run_geoclaw.sh jobs/my_args.txt
```
The parameter `--qos standby` means that you may run the jobs in a "standby" queue, i.e., jobs may
be preempted at any time and will resume operation once they are restarted.

For good use of compute node resources geoclaw needs to be recompiled for more OpenMP threads. This is easiest through a quick run with a `my_args.txt` file with the one following line:
```
--recompile --resolution 300 --bounds B-122.07999,17.24871,-99.20202,36.85008
```

The log outputs of the batch jobs are written to files in the `log/` subdirectory, and there are several scripts to help
you extract more information from the log outputs. For example, `python -m tc_inund.runtimes` will extract a job's run
time, and `python -m tc_inund.store_meta` identifies the (internal) location of the GeoClaw run directory and copies
central GeoClaw configuration files to the `output/` directory. Finally, `tc_inund.check_failed_jobs` will list all jobs
that ended with an error message. It will also help you to understand the error messages, and restart the jobs if you
think that might help (e.g., in cases where you modified the code).
