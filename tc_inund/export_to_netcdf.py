
import argparse
import pathlib

import rioxarray
import xarray as xr

import tc_inund.util.io as u_io


def _path_to_storm_id(path):
    return path.stem.split("_")[0]


def main():
    parser = argparse.ArgumentParser(description=(
        'Combine previously computed inundated areas for several events in a single NetCDF file.'
    ))
    parser.add_argument('input_tif', metavar="PATH", nargs="+", type=pathlib.Path,
                        help='Path to the GeoTIFF files to combine.')
    parser.add_argument('output_nc', metavar="PATH", type=pathlib.Path,
                        help='Name of the output NetCDF file.')
    args = parser.parse_args()

    if args.output_nc.exists():
        raise FileExistsError(
            f"The specified output NetCDF file already exists: {args.output_nc}"
        )

    input_sorted = sorted(args.input_tif, key=_path_to_storm_id)

    ds = []
    for path in input_sorted:
        ds.append(
            rioxarray.open_rasterio(path, band_as_variable=True)
            .rename({"band_1": "height", "x": "lon", "y": "lat"})
            .drop_vars(["spatial_ref"])
        )
    ds = xr.combine_nested(ds, "event")

    ds["storm_id"] = ("event", [_path_to_storm_id(p) for p in input_sorted])

    ds["lat"].attrs.update({
        "units": "degrees_north",
        "standard_name": "latitude",
    })

    ds["lon"].attrs.update({
        "units": "degrees_east",
        "standard_name": "longitude",
    })

    ds["height"].attrs.update({
        "units": "meters",
        "standard_name": "TC surge inundation height above ground",
    })

    ds["storm_id"].attrs.update({
        "units": "IBTrACS storm ID",
    })

    u_io.write_safely(ds, args.output_nc)


if __name__ == "__main__":
    main()
