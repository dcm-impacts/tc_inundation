
import argparse
import getpass
import tarfile
import urllib.request

import tc_inund.util.constants as u_const


AVISO_FTP_HOST = "ftp-access.aviso.altimetry.fr"

AVISO_FTP_DIR = "auxiliary/tide_model/fes2014_elevations_and_load"

AVISO_FES_GIT_COMMIT = "8a5debcab4a94fb140fc7057544a81b021dbe139"

AVISO_FES_GIT_URL = f"https://github.com/CNES/aviso-fes/raw/{AVISO_FES_GIT_COMMIT}/data/fes2014"


def download_aviso_ftp(ftp_user, ftp_pass, ftp_path, ftp_file, output_path):
    print(f"Downloading {ftp_file} with AVISO account credentials ...")
    filename, headers = urllib.request.urlretrieve(
        f"ftp://{ftp_user}:{ftp_pass}@{AVISO_FTP_HOST}/{ftp_path}/{ftp_file}"
    )
    with tarfile.open(filename) as tar:
        tar.extractall(path=output_path)


def download_aviso_git(filename, output_path):
    url = f"{AVISO_FES_GIT_URL}/{filename}"
    print(f"Downloading {filename} from GitHub ...")
    urllib.request.urlretrieve(url, filename=output_path / filename)


def main():
    parser = argparse.ArgumentParser(description='Download FES2014 input data.')
    parser.add_argument('--username', type=str, metavar="USERNAME", default="",
                        help='AVISO account username for FTP access.')
    parser.add_argument('--password', type=str, metavar="PASSWORD", default="",
                        help='AVISO account password for FTP access.')
    args = parser.parse_args()

    if args.username == "":
        print("Create a free account on the AVISO website if you do not have one already:")
        print("https://www.aviso.altimetry.fr/en/data/data-access/registration-form.html")
        ftp_user = input("Enter your AVISO account username: ")
    else:
        ftp_user = args.username

    if args.password == "":
        ftp_pass = getpass.getpass(prompt="Enter your AVISO account password: ")
    else:
        ftp_pass = args.password

    output_path = u_const.INPUT_DIR / "fes2014"
    output_path.mkdir(parents=True, exist_ok=True)

    download_aviso_git("load_tide.ini", output_path)
    download_aviso_git("ocean_tide_extrapolated.ini", output_path)

    download_aviso_ftp(
        ftp_user,
        ftp_pass,
        f"{AVISO_FTP_DIR}/fes2014b_elevations_extrapolated",
        "ocean_tide_extrapolated.tar.xz",
        output_path,
    )

    download_aviso_ftp(
        ftp_user,
        ftp_pass,
        f"{AVISO_FTP_DIR}/fes2014a_loadtide",
        "load_tide.tar.xz",
        output_path,
    )


if __name__ == "__main__":
    main()
