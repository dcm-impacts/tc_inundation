
import argparse
import contextlib
import os
import pathlib
import pickle
import sys
import warnings

from climada.hazard import Centroids, TCTracks
import climada.util.coordinates as u_coord
from climada_petals.hazard.tc_surge_geoclaw import TCSurgeGeoClaw
from climada_petals.hazard.tc_surge_geoclaw.sea_level_funs import (
    _get_closest_valid_cell,
    _nc_rename_vars,
    _sea_level_nc_info,
    area_sea_level_from_monthly_nc,
)
import numpy as np
import pyfes
import rasterio
import xarray as xr


import tc_inund.util.constants as u_const


@contextlib.contextmanager
def _filter_xr_warnings():
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            message="distutils Version classes are deprecated",
            category=DeprecationWarning,
        )
        warnings.filterwarnings(
            "ignore",
            message="Index.ravel returning ndarray is deprecated; in a future version",
            module="xarray",
            category=FutureWarning,
        )
        warnings.filterwarnings(
            "ignore",
            message="invalid value encountered in cast",
            module="xarray",
            category=RuntimeWarning,
        )
        yield


class FESReader:
    def __init__(self):
        ocean_tide_path = u_const.FES_DIR / "ocean_tide_extrapolated.ini"
        load_tide_path = u_const.FES_DIR / "load_tide.ini"
        self.short_tide = pyfes.Handler("ocean", "io", str(ocean_tide_path))
        self.radial_tide = pyfes.Handler("radial", "io", str(load_tide_path))

    def calculate(self, lons, lats, dates):
        lons, lats, dates = np.broadcast_arrays(lons, lats, dates)
        tide, lp, _ = self.short_tide.calculate(lons, lats, dates)
        load, _, _ = self.radial_tide.calculate(lons, lats, dates)
        # sum up and convert cm to m
        return (tide + lp + load) / 100.0

    def calculate_period(self, lons, lats, period, t_res_h=1):
        t_res = np.timedelta64(t_res_h, 'h')
        dates = np.arange(period[0], period[1] + t_res, t_res).astype('datetime64[us]')
        return self.calculate(lons, lats, dates)


def mean_from_zos_nc(path, period, lon, lat):
    if path is None:
        return lon, lat, 0
    with _filter_xr_warnings(), xr.open_dataset(path) as ds:
        da_zos = _nc_rename_vars(ds)["zos"]
        da_zos = da_zos.sel(time=(da_zos["time"] >= period[0]) & (da_zos["time"] <= period[1]))
        lon, lat = _get_closest_valid_cell(da_zos, lon, lat)
        v_mean = da_zos.sel(lon=lon, lat=lat).mean().item()
    return lon, lat, v_mean


def sea_level_from_fes(zos_path, mod_zos=0, mode="mean"):
    if zos_path is not None:
        _sea_level_nc_info(zos_path)
    reader = FESReader()
    def sea_level_fun(bounds, period, zos_path=zos_path, reader=reader, mod_zos=mod_zos):
        centroid = (0.5 * (bounds[0] + bounds[2]), 0.5 * (bounds[1] + bounds[3]))
        period_year = (period[1] - np.timedelta64(366, "D"), period[1])

        lon, lat, nc_yrmean = mean_from_zos_nc(zos_path, period_year, *centroid)
        fes_yrmean = reader.calculate_period(lon, lat, period_year).mean()
        offset = nc_yrmean - fes_yrmean + mod_zos

        fes_value = getattr(reader.calculate_period(lon, lat, period), mode)()
        return fes_value + offset
    return sea_level_fun


def haz_to_raster(haz, path):
    """Convert point centroids to a regular grid and store as GeoTIFF file

    The raster specification is guessed from the distance between the centroids

    Parameters
    ----------
    haz : Hazard
        Object of CLIMADA's Hazard type.
    path : Path or str
        Path to a GeoTIFF file for raster output.
    """
    haz.centroids.set_lat_lon_to_meta()
    haz.centroids.meta['compress'] = 'deflate'
    assigned_idx = u_coord.match_grid_points(
        haz.centroids.lon,
        haz.centroids.lat,
        haz.centroids.meta['width'],
        haz.centroids.meta['height'],
        haz.centroids.meta['transform'],
    )
    intensity = np.zeros(
        haz.centroids.meta['height'] * haz.centroids.meta['width'],
        dtype=np.float64,
    )
    intensity[assigned_idx] = haz.intensity.toarray()
    u_coord.write_raster(path, intensity[None], haz.centroids.meta)


def centroids_from_bounds(bounds, res_as=30):
    res_deg = res_as / (60 * 60)
    global_origin = (-180, 90)
    global_transform = rasterio.transform.from_origin(*global_origin, res_deg, res_deg)

    cens = []
    for bnd in bounds:
        centroids = Centroids()
        transform, (height, width) = u_coord.subraster_from_bounds(global_transform, bnd)
        centroids.meta = {
            'width': width,
            'height': height,
            'crs': centroids.crs,
            'transform': transform,
        }
        centroids.set_meta_to_lat_lon()
        centroids.meta = {}
        cens.append(centroids)
    return Centroids.union(*cens)


def modify_storm_intensity(ds, factor):
    ds['max_sustained_wind'] *= factor
    penv, pcen = ds.environmental_pressure, ds.central_pressure
    ds['central_pressure'] += (1 - factor) * (penv - pcen)


def main():
    parser = argparse.ArgumentParser(description='Compute areas inundated by TC storm event.')
    parser.add_argument('storm_id', metavar="STORM_ID",
                        help='The IBTrACS storm ID of the storm.')
    float_tuple = lambda arg: tuple([float(x) for x in arg.strip("pPbB").split(",")])
    parser.add_argument('--bounds', type=float_tuple,
                        metavar="LON_MIN,LAT_MIN,LON_MAX,LAT_MAX", nargs="+",
                        default=[(-180, -90, 180, 90)],
                        help=('Geographical bounds. Prefix negative LON_MIN-values with "B" '
                              'to avoid interpretation as arguments.'))
    parser.add_argument('--time', type=np.datetime64, metavar="DATE", nargs=2, default=None,
                        help='Time window to consider (begin, end).')
    parser.add_argument('--gauges', type=float_tuple, metavar="LAT,LON", nargs="+", default=None,
                        help=('Geographical coordinates of tide gauges (lat-lon-pairs). '
                              'Prefix negative LAT-values with "P" to avoid interpretation '
                              'as arguments.'))
    parser.add_argument('--zos', type=str, metavar="ZOS_NAME", default="aviso",
                        help='Read base sea level from this data source.')
    parser.add_argument('--dem', type=str, metavar="DEM_VERSION", default="v2.1",
                        help='Read DEM from this data source version.')
    parser.add_argument('--tides', metavar="MODE", choices=["no", "min", "mean", "max"], default="no",
                        help='Use FES2014 tide model to determine base sea level (min/mean/max).')
    parser.add_argument('--resolution', type=int, metavar="ARC_SECONDS", default=30,
                        help='Resolution of the topography in arcseconds. Tested values are 3,9,30,60,90,300. Values smaller 3 lead to instability.')
    parser.add_argument('--suffix', type=str, metavar="STR", default="",
                        help='Extra suffix for file name.')
    parser.add_argument('--bc', type=str, metavar="BOUNDARY_CONDITIONS", default="extrap",
                        help='Boundary conditions, one of "extrap", "periodic", or "wall".')
    parser.add_argument('--mod_zos', type=float, metavar="VALUE", default=0,
                        help='Additional scalar sea level rise in meters.')
    parser.add_argument('--mod_intensity', type=float, metavar="FACTOR", default=1,
                        help='Relative change in storm intensity.')
    parser.add_argument('--output-interval', type=int, metavar="INTERVAL_SEC", default=0,
                        help='Intervals between GeoClaw output times (for debug use) in seconds. Choose 0 for no intermediate output.')
    parser.add_argument('--recompile', action="store_true",
                        help='Enforce recompilation of Clawpack.')
    args = parser.parse_args()

    storm_id = args.storm_id
    bounds = args.bounds
    time = args.time
    gauges = args.gauges
    dem = args.dem
    zos_name = args.zos
    tides = args.tides
    topo_res_as = args.resolution
    mod_intensity = args.mod_intensity
    boundary_conditions = args.bc
    suffix = args.suffix
    mod_zos = args.mod_zos
    output_freq_s = (
        1.0 / args.output_interval
        if args.output_interval > 0.0 else
        0.0
    )
    recompile = args.recompile

    suffix = f"{suffix}-zos_{zos_name}-fes_{tides}"
    pool = None
    if 'SLURM_NTASKS' in os.environ and int(os.environ['SLURM_NTASKS']) > 1:
        from mpi4py.futures import MPIPoolExecutor
        pool = MPIPoolExecutor()
        # MPI is not working well at the moment, so don't overwrite existing files...
        suffix = f"{suffix}-mpi"

    if pool is not None:
        pool.shutdown()

    file_stem = f"{storm_id}{suffix}"
    out_dir = u_const.OUTPUT_DIR / file_stem
    out_dir.mkdir(parents=True, exist_ok=True)
    path_resume = out_dir / f"{file_stem}-resume.txt"
    path_gaugedata = out_dir / f"{file_stem}-gauge_data.pickle"
    path_hdf5 = out_dir / f"{file_stem}.hdf5"
    path_tif = out_dir / f"{file_stem}.tif"

    if path_tif.exists():
        print(f"Skip already existent {out_dir} ...")
        return

    with _filter_xr_warnings():
        tracks = TCTracks.from_ibtracs_netcdf(storm_id=storm_id, estimate_missing=True)

    if time is not None:
        ds = tracks.data[0]
        t_mask = (time[0] <= ds.time) & (ds.time <= time[1])
        tracks.data = [ds.sel(time=t_mask)]

    if tracks.size == 0:
        sys.stderr.write(f"No valid storm data for {storm_id}, aborting ...\n")
        return

    if mod_intensity != 1:
        modify_storm_intensity(tracks.data[0], mod_intensity)

    zos_path = u_const.ZOS_FILE(zos_name)
    if tides == "no":
        if zos_name == "0":
            sea_level_fun = 0
        else:
            sea_level_fun = area_sea_level_from_monthly_nc(zos_path, mod_zos=mod_zos)
    else:
        if zos_name == "0":
            zos_path = None
        sea_level_fun = sea_level_from_fes(zos_path, mod_zos=mod_zos, mode=tides)

    haz = TCSurgeGeoClaw.from_tc_tracks(
        tracks,
        centroids_from_bounds(bounds, res_as=topo_res_as),
        u_const.TOPO_FILE[dem],
        geoclaw_kwargs=dict(
            topo_res_as=topo_res_as,
            gauges=gauges,
            sea_level=sea_level_fun,
            boundary_conditions=boundary_conditions,
            output_freq_s=output_freq_s,
            recompile=recompile,
            resume_file=path_resume,
        ),
        pool=pool,
    )

    if gauges is not None:
        with open(path_gaugedata, "wb") as fp:
            pickle.dump(haz.gauge_data, fp)
    haz.write_hdf5(path_hdf5)
    haz_to_raster(haz, path_tif)


if __name__ == "__main__":
    main()
