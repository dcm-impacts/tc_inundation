
import pathlib

import tc_inund


BASE_DIR = pathlib.Path(tc_inund.__file__).resolve().parent.parent
"""Base directory for all paths defined below (usually the repository's root)

Note that the __file__ attribute points to the "tc_inund/__init__.py" file.
"""

INPUT_DIR = BASE_DIR / "input"

OUTPUT_DIR = BASE_DIR / "output"

LOG_DIR = BASE_DIR / "log"

JOBS_DIR = BASE_DIR / "jobs"

ZOS_FILE = lambda name: INPUT_DIR / f"monthly_zos_{name}.nc"

ZOS_DAILY_DIR = INPUT_DIR / "daily_zos_aviso"

DEM_DIR = INPUT_DIR / "dem"

TOPO_FILE = {
    "v1.1": DEM_DIR / "combined" / "combine_v1.1.vrt",
    "v2.1": DEM_DIR / "combined" / "combine_v2.1.vrt",
    "srtm": DEM_DIR / "srtm15plus" / "index.vrt",
}

FES_DIR = INPUT_DIR / "fes2014"

STDOUT_PATTERN_RUNPATH = r"/p/tmp/tovogt/.climada/data/geoclaw/runs/[0-9]{4}-[0-9NS-]+/"
