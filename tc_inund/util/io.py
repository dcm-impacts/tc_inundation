
import datetime as dt
import hashlib
import pathlib
import signal
import sys


class GracefulInterruptHandler():
    """Context manager for handling SIGTERM (e.g. when running in `standby` QOS on SLURM)

    Example
    -------
    >>> with GracefulInterruptHandler() as h:
    >>>     # do something that may not be interrupted ...
    >>>     if h.interrupted:
    >>>         print("Quitting gracefully after SIGTERM ...")
    >>>         sys.exit()
    >>> # if there was no interruption, the code continues here
    """
    def __init__(self, sigs=[signal.SIGTERM, signal.SIGINT]):
        self.sigs = sigs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        self.original_handlers = []
        for sig in self.sigs:
            self.original_handlers.append(signal.getsignal(sig))
            signal.signal(sig, self.handle)

        return self

    def __exit__(self, type, value, tb):
        self.release()

    def handle(self, signum, frame):
        print(f"[{dt.now()}] Signal ({signum}) received. Cleaning up ...")
        self.release()
        self.interrupted = True

    def release(self):
        if self.released:
            return False

        for sig, handler in zip(self.sigs, self.original_handlers):
            signal.signal(sig, handler)

        self.released = True
        return True


def path_exists_safely(path, delete=True):
    """Check if file exists and is not corrupt

    For data that has been written using `write_safely`, this will check whether the file and
    its checksum file exist and whether the checksum matches. If not, the data is considered as
    corrupted and the file and its checksum file are removed (if they exist).

    An empty file (size 0) is considered to be non-existent, even if the checksum file matches.

    Parameters
    ----------
    path : Path
        The path to the file to check.
    delete : bool, optional
        If False, do not remove the file and its checksum file in case of corruption.
        Default: True

    Returns
    -------
    bool
    """
    checksum_path = path.parent / f"{path.name}.sha256sum"
    if not path.exists() or path.stat().st_size == 0:
        exists = False
    else:
        exists = check_sha256sum(path)
    if delete and not exists:
        path.unlink(missing_ok=True)
        checksum_path.unlink(missing_ok=True)
    return exists


def write_safely(ds, path, encoding=None):
    """Write NetCDF data together with checksum file

    If you later find that the file exists, but the checksum file doesn't exist or doesn't match,
    then the data is probably corrupted, e.g. because the process was killed during writing.

    Parameters
    ----------
    ds : xr.Dataset
        The dataset to store in the given location.
    path : Path
        The path to the destination file.
    encoding : dict, optional
        In case of xr.Dataset, pass on to the `to_netcdf` function. If not specified otherwise,
        all data variables are compressed by default (zlib=True). Default: None

    See also
    --------
    path_exists_safely
    """
    encoding = {} if encoding is None else encoding
    with GracefulInterruptHandler() as h:
        print(f"Writing to {path.name} ... ", end="")
        path.parent.mkdir(parents=True, exist_ok=True)
        for v in ds.variables:
            if ds[v].ndim <= 1 or ds[v].dtype.kind in ["U", "O"]:
                continue
            encoding[v] = {"zlib": True, **encoding.get(v, {})}
        ds.to_netcdf(path, format="NETCDF4", engine="netcdf4", encoding=encoding)
        write_sha256sum(path)
        print("done.")
        if h.interrupted:
            print("Quitting gracefully after SIGTERM ...")
            sys.exit()


def check_sha256sum(path):
    """Compare the SHA256 checksum to that stored in the accompanying *.sha256sum file

    Returns False, if the *.sha256sum does not exist or contains a reference to a different path.

    Parameters
    ----------
    path : Path or str
        Path to file.

    Returns
    -------
    bool
    """
    checksum_file = path.parent / f"{path.name}.sha256sum"
    if not checksum_file.exists():
        return False
    checksum_data = checksum_file.read_text().strip().split()
    if len(checksum_data[0]) != 64:
        return False
    if len(checksum_data) >= 2 and pathlib.Path(checksum_data[1]).name != path.name:
        return False
    checksum = compute_sha256sum(path)
    return checksum == checksum_data[0]


def write_sha256sum(path):
    """Write the SHA256 checksum to a *.sha256sum file

    Parameters
    ----------
    path : Path or str
        Path to file.
    """
    checksum_file = path.parent / f"{path.name}.sha256sum"
    checksum = compute_sha256sum(path)
    checksum_file.write_text(f"{checksum} {path.name}")


def compute_sha256sum(path):
    """Compute the SHA256 checksum of a file

    Parameters
    ----------
    path : Path or str
        Path to file.

    Returns
    -------
    str
    """
    h = hashlib.sha256()
    chunk_num_blocks = 128
    chunk_size = chunk_num_blocks * h.block_size
    with open(path, "rb") as fp:
        while chunk := fp.read(chunk_size):
            h.update(chunk)
    return h.hexdigest()
