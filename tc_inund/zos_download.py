
import cdsapi
from multiprocessing import Pool
import pathlib
import subprocess
import zipfile

import tc_inund.util.constants as u_const


def compile_cds_request(year, month):
    return {
        'version': 'vDT2021',
        'format': 'zip',
        'variable': 'daily',
        'year': f"{year:04d}",
        'month': f"{month:02d}",
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
    }


def retrieve_combine_and_rm(cds_name, cds_request, outpath):
    zippath = outpath.with_suffix(".zip")
    unzippath = outpath.with_suffix("")
    unzippath.mkdir(parents=True, exist_ok=True)

    if not zippath.exists():
        c = cdsapi.Client()
        c.retrieve(cds_name, cds_request, zippath)

    with zipfile.ZipFile(zippath, 'r') as zipf:
        zipf.extractall(unzippath)

    inpaths = sorted(unzippath.iterdir())
    proc = subprocess.Popen(
        ["cdo", "-f", "nc4", "-z", "zip", "-copy", "-mergetime"]
        + inpaths + [outpath]
    )
    proc.wait()

    zippath.unlink()
    for p in inpaths:
        p.unlink()
    unzippath.rmdir()


def main():
    u_const.ZOS_DAILY_DIR.mkdir(parents=True, exist_ok=True)
    period = (1993, 2022)
    cds_name = "satellite-sea-level-global"

    args = []
    for year in range(period[0], period[1] + 1):
        for month in range(1, 13):
            outpath = u_const.ZOS_DAILY_DIR / f"dt_global_twosat_phy_l4_{year:04d}{month:02d}_vDT2021.nc"
            if outpath.exists():
                continue
            print(outpath.name)
            cds_request = compile_cds_request(year, month)
            args.append((cds_name, cds_request, outpath))

    with Pool(processes=5) as pool:
        pool.starmap(retrieve_combine_and_rm, args)


if __name__ == "__main__":
    main()
