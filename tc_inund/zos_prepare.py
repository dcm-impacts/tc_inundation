
import pathlib

import climada.util.coordinates as u_coord
import numpy as np
import xarray as xr

import tc_inund.util.constants as u_const
import tc_inund.util.io as u_io


GEOID_FILE = u_const.INPUT_DIR / "GOCO05s_sub_EGM96_900as.nc"
"""Add this to data that is relative to GOCO05s to obtain data that is relative to EGM96."""


def equalize_coords(ds, ds_ref):
    """Set data set's coordinate names and values to those of the reference data set

    If the difference between the old and new coordinate values is larger than half a grid cell, an
    Exception is raised.

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        A data set (or array) with dimensions called "lon" (or "longitude") and "lat" (or
        "latitude"), other dimensions remain unchanged.
    ds_ref : xr.Dataset or xr.DataArray
        A data set (or array) with dimensions called "lon" (or "longitude") and "lat" (or
        "latitude"). This grid is expected to have the same shape as the grid in `ds`, and the
        values should only differ non-significantly (up to lon-normalization).

    Returns
    -------
    xr.Dataset or xr.DataArray
    """
    c_names_ref = []
    for c_names in [["lon", "longitude"], ["lat", "latitude"]]:
        c_name = [c for c in c_names if c in ds.coords][0]
        c_name_ref = [c for c in c_names if c in ds_ref.coords][0]
        if c_name != c_name_ref:
            ds = ds.rename({c_name: c_name_ref})
        c_names_ref.append(c_name_ref)
    c_lon, c_lat = c_names_ref

    mid_lon = np.mean([ds_ref[c_lon].values.min(), ds_ref[c_lon].values.max()])
    lons = u_coord.lon_normalize(ds[c_lon].values.copy(), center=mid_lon)
    ds = ds.assign_coords({c_lon: lons}).reindex({c_lon: np.unique(lons)})

    for c in [c_lat, c_lon]:
        res = np.abs(ds[c].values[1] - ds[c].values[0])
        assert np.abs(ds[c].values - ds_ref[c].values).max() < 0.05 * res
        ds = ds.assign_coords({c: ds_ref[c].values})
    return ds


def main():
    period = (1993, 2022)
    paths = [
        u_const.ZOS_DAILY_DIR / f"dt_global_twosat_phy_l4_{year:04d}{month:02d}_vDT2021.nc"
        for year in range(period[0], period[1] + 1)
        for month in range(1, 13)
    ]
    ds = xr.open_mfdataset(paths)[["adt"]].resample(time="1MS").mean(dim="time")
    da_geoid = equalize_coords(xr.open_dataset(GEOID_FILE)["geoid_height"], ds)
    ds["adt"] += da_geoid
    u_io.write_safely(ds.compute(), u_const.ZOS_FILE("aviso"))


if __name__ == "__main__":
    main()
